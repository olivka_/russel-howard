<div class="social">
  <?php
  foreach (\theme\Social::$socials as $socialName) {
    $id = strtolower($socialName) . '_url';
    $url = get_theme_mod($id, '');

    if ($url) { ?>
      <a target="_blank" href="<?php echo $url ?>" class="social_link <?php echo $id ?>"></a>
      <?php
    }
  }
  ?>
</div>
<div class="copy-row">
  <?php
  $copy = get_theme_mod('copyright', '');
  echo $copy;
  ?>
</div>
<div class="footer-links">
  <?php
  wp_nav_menu(array(
    'menu' => 'Footer'
  ))
  ?>
</div>