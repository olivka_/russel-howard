<?php

namespace theme;

class Links {
  public $showLinks = true;
  public $showCounter = false;

  public $title = '';
  public $description = '';

  public $links = array();

  public function __construct() {
    $this->showLinks = get_field('show_section');
    //$this->showCounter = apply_filters('countdown_shortcode', $this->showCounter);

    if ($this->showLinks) {
      $this->title = get_field('links_title');
      $this->description = get_field('links_description');
      $this->links = get_field('links');
    }
  }


  /**
   * @return array
   */
  public function getLinks() {
    return is_array($this->links) ? $this->links : array();
  }
}