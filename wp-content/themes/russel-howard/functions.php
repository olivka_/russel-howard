<?php
require_once 'inc/Links.php';
require_once 'inc/Social.php';

function themeSetup() {
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');

  register_nav_menus(array(
    'top' => __('Top Menu', 'twentyseventeen'),
    'social' => __('Social Links Menu', 'twentyseventeen'),
  ));

  add_theme_support('html5', array(
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  add_theme_support('custom-logo', array(
    'flex-width' => true,
  ));
}

add_action('after_setup_theme', 'themeSetup');


add_action('wp_enqueue_scripts', function () {
  wp_enqueue_style('iconfont', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array('design'), 1);
  wp_enqueue_style('design', get_template_directory_uri() . '/assets/css/design.css', array(), 1);
  wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), 1);

  wp_deregister_style('uji-countdown-uji-countdown');
});

show_admin_bar(0);

add_filter('banner_class', function ($class, $imageId, $imageMobileId) {

  if ($imageId) {
    $class .= ' has-desktop-image';
  } else {
    $class .= ' no-desktop-image';
  }

  if ($imageMobileId) {
    $class .= ' has-mobile-image';
  } else {
    $class .= ' no-mobile-image';
  }


  return $class;
}, 10, 3);

function theme_customize($wp_customize) {
  /**
   * @var WP_Customize_Manager $wp_customize
   */

  $wp_customize->add_section(
    'theme_socials',
    array(
      'title' => 'Theme socials',
      'priority' => 35,
    )
  );

  foreach (\theme\Social::$socials as $socialName) {
    $id = strtolower($socialName) . '_url';

    $wp_customize->add_setting($id);

    $wp_customize->add_control($id,
      array(
        'label' => $socialName . ' url',
        'section' => 'theme_socials',
        'type' => 'url',
      )
    );
  }


  $wp_customize->add_section(
    'copyright',
    array(
      'title' => 'Copyright',
      'priority' => 35,
    )
  );
  $wp_customize->add_setting('copyright', array(
    'default' => '&copy; Russel Howard. All Rights Reserved.'
  ));

  $wp_customize->add_control('copyright',
    array(
      'label' => 'Copyright',
      'section' => 'copyright',
      'type' => 'textarea',
    )
  );

}

add_action('customize_register', 'theme_customize');


