<?php
if (is_front_page()) return;
?>

<div class="logo-row">
  <div class="logo">
    <?php the_custom_logo(); ?>
  </div>
</div>
