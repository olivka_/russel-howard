<?php

namespace theme;

class Social {
  public static $socials = array(
    'Facebook',
    'Twitter',
    'Instagram',
    'YouTube'
  );
}