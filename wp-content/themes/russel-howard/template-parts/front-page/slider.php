<?php

$title = get_field('title');
$description = get_field('description');

$imageId = get_field('image');
$imageMobileId = get_field('image_mobile');

?>


<div class="home-banner">
  <div class="<?php echo apply_filters('banner_class', 'image', $imageId, $imageMobileId) ?>">
    <?php if ($imageId) { ?>
      <?php echo wp_get_attachment_image($imageId, 'full', false, array(
        'class' => 'image-desktop'
      )) ?>
    <?php } ?>
    <?php
    if ($imageMobileId) { ?>
      <?php echo wp_get_attachment_image($imageMobileId, 'full', false, array(
        'class' => 'image-mobile'
      )) ?>
    <?php }
    if (false) { ?>
      <div class="text">
        <h1><?php echo $title ?></h1>
        <div class="description"><?php echo $title ?></div>
      </div>
    <?php } ?>
  </div>
</div>