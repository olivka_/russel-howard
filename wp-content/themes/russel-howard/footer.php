<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="footer__content mw">
    <?php get_template_part('template-parts/global/footer'); ?>
  </div>
</footer>
</div>
</div>
<?php wp_footer(); ?>

</body>
</html>
