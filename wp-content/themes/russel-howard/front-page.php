<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <?php // Show the selected frontpage content.
      if (have_posts()) :
        while (have_posts()) : the_post(); ?>
          <div class="header-content">
            <?php get_template_part('template-parts/front-page/slider'); ?>
            <?php get_template_part('template-parts/global/content'); ?>
          </div>
          <?php get_template_part('template-parts/front-page/links');
        endwhile;
      else :
      endif; ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer();
