<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'russel-howard');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2;)_GiL+^0@s.%<$]sjGh|(nrqpk?*Vj<sj%A L(KH)$;A+Yi#j?6v?B7Xf 0+.x');
define('SECURE_AUTH_KEY',  '%>,m,LW!0K|mQIbv%sQWm]?}Z`3GCqy;=%SI``qcN_onp,Y^M.Rv{ TZ g^Ud8|L');
define('LOGGED_IN_KEY',    '.g@[5@rl6k.V_X_YFT2iLJ??NpA[7U7iYjaUXb2a!=Ecyv^+$iT;.C~p&by%#r &');
define('NONCE_KEY',        '3/XdY`AR#GSfooPG^/z ~?#nPBYt:|Q`1{xW(10VR?e^x|IO#3FG!_l98#X4Ru#z');
define('AUTH_SALT',        '&+{{8(nv)8B!^5ZPUP7VI<yE$%p5j<]EnNnr3aWF|EOhfi#+acKb<Q{~^YYsbFiV');
define('SECURE_AUTH_SALT', 'h<DpV^05q.=t-`:%bYgxSeNX:xvV+~_tNgxJH_F).e#|=[7%yor/&blk%0L&FkfT');
define('LOGGED_IN_SALT',   '9<5O[d5D4VY3_LZSabHOm&G%+KL>g([MZ735Jzqi3,*=e-W797&>~5a!0)RzAy{B');
define('NONCE_SALT',       '<)vhA}}2Y=uN- @I+&`^kz&p%kc1WgBlzn}pF8GEwY1<78ol@?WkL0{T/IHi/J}#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
