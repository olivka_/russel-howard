<?php

$links = new \theme\Links();

if (!$links->showLinks || empty($links->getLinks())) {
  return;
}

?>

<div class="show-links">
  <div class="mw">
    <div class="show-links__content">
      <h2><?php echo $links->title ?></h2>
      <div class="description"><?php echo $links->description ?></div>
      <div class="links">
        <?php foreach ($links->getLinks() as $link) { ?>
          <div class="link">
            <div class="show-info">
              <div class="show-row-1">
                <div class="date"><?php echo $link['date'] ?></div>
              </div>
              <div class="show-row-2">
                <div class="show-venue"><?php echo $link['venue'] ?></div>
                <div class="show-location"><?php echo $link['location'] ?></div>
              </div>
            </div>
            <div class="show-button">
              <a class="button" href="<?php echo $link['link_url'] ?>"
                 target="_blank"><?php echo $link['link_text'] ?></a>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
